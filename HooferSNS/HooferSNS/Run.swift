//
//  Run.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/7/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import Foundation
import CoreData

class Run {
    
     var duration = ""
     var avgSpeed = ""
     var maxSpeed = ""
     var altChange = ""
     var distance = ""
     var owner = ""
     var timestamp = NSDate()
     var locations = NSOrderedSet()
    
}

