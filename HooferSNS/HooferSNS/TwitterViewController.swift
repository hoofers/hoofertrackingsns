//
//  TwitterViewController.swift
//  HooferSNS
//
//  Created by Sophia Ehlen on 12/12/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import TwitterKit


class TwitterViewController: TWTRTimelineViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let client = TWTRAPIClient()
        self.dataSource = TWTRUserTimelineDataSource(screenName: "hoofer_sns", APIClient: client)
        
        TWTRTweetView.appearance().primaryTextColor = UIColor.blackColor()
        
        //Change background colors to blend with home screen image
        TWTRTweetView.appearance().backgroundColor = UIColor(red: (244/255.0), green: (244/255.0), blue: (244/255.0), alpha: 1.0)
        self.view.backgroundColor = UIColor(red: (244/255.0), green: (244/255.0), blue: (244/255.0), alpha: 1.0)
        
        
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
