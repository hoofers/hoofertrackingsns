//
//  HomeViewController.swift
//  HooferSNS
//
//  Created by Gaoyuan Chen and Sophia Ehlen on 12/2/15.
//  Copyright © 2015 Gaoyuan Chen and Sophia Ehlen. All rights reserved.
//

import UIKit
import Parse
import SwiftyJSON
import Alamofire
import AlamofireImage
import TwitterKit

import Social
import SafariServices

class HomeViewController: UIViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var condition: UILabel!
    @IBOutlet weak var icon: UIImageView!
    

    
    
    // Show HooferSNS website if user pushes button to do so
    @IBAction func website_button(sender: AnyObject) {
        let safariVC = SFSafariViewController(URL:NSURL(string: "http://hoofersns.org")!, entersReaderIfAvailable: false)
        safariVC.delegate = self
        self.presentViewController(safariVC, animated: true, completion: nil)
        func safariViewControllerDidFinish(controller: SFSafariViewController) {
            controller.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    @IBOutlet weak var TwitterView: UIView!
    @IBAction func signInAndLogOut(sender: UIBarButtonItem) {
        if(PFUser.currentUser() == nil){
            
            performSegueWithIdentifier("logInFromHome", sender: self)
            
        }else{
            performSegueWithIdentifier("logOut", sender: self)
            self.navigationItem.rightBarButtonItem?.title = "Sign In"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Weather image styling
        self.icon.layer.backgroundColor = UIColor.whiteColor().CGColor
        icon.layer.cornerRadius = 35;
        icon.layer.borderWidth = 2.0;
        icon.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        //Update UI - Navigation Bar and Background
        self.navigationController?.navigationBar.backgroundColor = UIColor.blackColor()
        self.view.backgroundColor = UIColor(red: (244/255.0), green: (244/255.0), blue: (244/255.0), alpha: 1.0)
        
        
        
        // Make App accessible through keyword search
        let myActivity = NSUserActivity(activityType: "com.CormickHnilicka.HooferSNS")
        myActivity.title = "HooferSNS"
        myActivity.eligibleForSearch = true
        myActivity.keywords = Set(arrayLiteral: "Hoofers", "HooferSNS", "SNS", "UW", "UW Hoofers")
        self.userActivity = myActivity
        myActivity.eligibleForHandoff = false
        myActivity.becomeCurrent()
        
    
        
        // Image url label for weather image
        var imageURL : String = "no url"

        
        print(PFUser.currentUser())
       
        // Do any additional setup after loading the view.
        
        // JSON parsing for home page
        
        Alamofire.request(.GET, "http://api.wunderground.com/api/e9de73f691203607/conditions/q/WI/Madison.json")
            .responseJSON { response in
                //                debugPrint(response)
                //                print("Orig URL")
                //                print(response.request)
                //                print(response.response)
                //                print(response.data)
                //                print(response.result)
                
                if let value: AnyObject = response.result.value{
                    
                    let post = JSON(value)
                    
                    print("The post is " + post.description)
                    
                    // Parse the current weather conditions
                    if let weather = post["current_observation"]["weather"].string {
                        print("THE WEATHER IS "  + weather)
                        self.condition.text = weather
                        
                    }
                    else {
                        print("error parsing WEATHER")
                    }
                    // Parse the weather icon url
                    if let iconURL = post["current_observation"]["icon_url"].string {
                        print("THE ICON URL IS " + iconURL)
                        imageURL = String(iconURL)
                        //self.load_icon("iconURL")
                        print(imageURL)
                    }
                    else {
                        print("error parsing icon url")
                    }
                    
                    
                    
                    // Parse the temperature
                    if let temp = post["current_observation"]["temp_f"].double {
                        let tempString = String(temp)
                        print("THE TEMP IS " +  tempString)
                        self.temp.text = tempString
                        self.temp.text?.appendContentsOf(" °F")
                    }
                    else {
                        print("error parsing temp")
                    }
                    
//                    // Parse the wind string
//                    if let windString = post["current_observation"]["wind_string"].string {
//                        print("THE WIND STRING IS " + windString)
//                        self.Wind.text = "Wind: "
//                        self.Wind.text?.appendContentsOf(windString)
//                    }
//                    else {
//                        print("error parsing windString")
//                    }
                    
                    
                    
                    
                }
                
                Alamofire.request(.GET, imageURL).responseImage { response in
                    debugPrint(response)
                    print(response.request)
                    print(response.response)
                    debugPrint(response.result)
                    
                    if let image = response.result.value {
                        print("Image downloaded: \(image)")
                        let url: NSURL = NSURL(string: imageURL)!
                        let size = CGSize(width: 50.0, height: 50.0)
                        let aspectScaledToFitImage = image.af_imageScaledToSize(size)
                        self.icon.image = image
                        
                    }
                    
                }
                
                
        }
        
        
        
        
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         
        
        if(PFUser.currentUser() !== nil){
            
            self.navigationItem.rightBarButtonItem?.title = "Log out"
            
        }else{
            
            self.navigationItem.rightBarButtonItem?.title = "Sign In"
            
        }
        self.navigationItem.setHidesBackButton(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "logInFromHome") {
            // pass data to next view
            var lvc = segue.destinationViewController as! LogInViewController;
            lvc.tabBarController?.hidesBottomBarWhenPushed = true
        }
        if (segue.identifier == "logOut") {
            // pass data to next view
            var lvc = segue.destinationViewController as! LogOutViewController;
            lvc.tabBarController?.hidesBottomBarWhenPushed = true
        }
    }
    
    @IBAction func unwindToHome(segue:UIStoryboardSegue) {
        if(segue.sourceViewController .isKindOfClass(LogInViewController))
        {
            self.navigationController?.navigationBarHidden = false
            
        }
        if(segue.sourceViewController .isKindOfClass(SignUpViewController))
        {
            self.navigationController?.navigationBarHidden = false
            
        }
        if(segue.sourceViewController .isKindOfClass(LogOutViewController))
        {
            self.navigationController?.navigationBarHidden = false
            
        }
    }



}
