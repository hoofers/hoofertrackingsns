//
//  GPSViewController.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/6/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse


class GPSViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var MapButton: UIButton!
    
    var locationmanager = CLLocationManager();
    
    var firstLocationUpdate: Bool?
    
    var didFindMyLocation = false
    
    var friendsLocs = [CLLocationCoordinate2D]()
    
    var userId = ""
    
    var lastUpdate = NSDate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        locationmanager = CLLocationManager();
        mapView.delegate = self;
        mapView.animateToLocation(CLLocationCoordinate2DMake(43.4753, -110.7692));
        mapView.animateToZoom(20);
        
        self.locationmanager.delegate = self;
        self.locationmanager.requestWhenInUseAuthorization();
        self.locationmanager.distanceFilter = 10
        self.locationmanager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationmanager.startUpdatingLocation()
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "homeBackground")?.drawInRect(self.view.bounds)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        
        //self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil);
        let user = PFUser.currentUser()
        if (user != nil){
            userId = (user?.objectId)!
        }
     
    }
    
    
    
    
    
    func findFriends(){
        var friends = [String]()
        let query = PFQuery(className: "followers")
        query.whereKey("follower", equalTo: (PFUser.currentUser()?.objectId)!)
        query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            if let objects = objects {
                
                for object in objects {
                    let temp = object["following"] as! (String)
                    friends.append(temp)
                    self.findUser(temp);
                }
            }
            
            
        })
       
    
    }
    
    func findUser(user: String) {
        let query = PFUser.query()
        query?.whereKey("objectId", equalTo: user)
        query?.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            if let objects = objects {
                
                for object in objects {
                    self.friends(object as! PFUser)
                }
            }
            
            
        })

    
    
    
    
    }
    func friends(user: PFUser) {
        let query = PFQuery(className: "CurrLoc");
        let temp = user
        query.whereKey("owner", equalTo: temp)
        query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            if let objects = objects {
                
                for object in objects {
                    let lat = object["latitude"] as! Double
                    let long = object["longitude"] as! Double
                    let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    let marker = GMSMarker(position: position)
                    marker.appearAnimation = kGMSMarkerAnimationPop;
                    marker.title = user["username"] as! String
                    marker.map = self.mapView
                  
                }
            }
            
            
        })

    
    
    
    
    }
    
    
    func isBetween(date date1: NSDate, date2: NSDate, date3: NSDate) -> Bool {
        let a = (date1.compare(date3) == date3.compare(date2))
            return a
        }
   
    func deleteOld(date: NSDate) -> Bool {
        let user = PFUser.currentUser()
        let query = PFQuery(className: "CurrLoc")
        var add = false;
        query.whereKey("owner", equalTo: (user)!)
        query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            
            if let objects = objects {
                
                for var i = 0; i < objects.count; i++ {
                    let timestamp = objects[i]["timestamp"] as! NSDate
                   
                    if (timestamp != date){
                    
                    objects[i].deleteInBackground()
                    add = true;
                    }
               
                    
                    
                }
                
                
            }

            
        })
        return add;
    }

    
    

    
    override func viewWillAppear(animated: Bool) {
        let user = PFUser.currentUser()
        if (user != nil){
        let currloc = PFObject(className: "CurrLoc");
        lastUpdate = NSDate()
        self.deleteOld(lastUpdate)
        currloc["timestamp"] = lastUpdate as! NSDate
        currloc["latitude"] = Double((locationmanager.location?.coordinate.latitude)!)
        currloc["longitude"] = Double((locationmanager.location?.coordinate.longitude)!)
        currloc["owner"] = PFUser.currentUser()
        currloc.saveInBackgroundWithBlock{
            (success: Bool, error: NSError?) -> Void in
            if (success){
                print("SAVED!")
            }
            else {
                print("fail")
            }
        }
        }
        

        super.viewWillAppear(animated)
        print("view will appear happened")
        if(PFUser.currentUser() != nil){
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Friends", style: UIBarButtonItemStyle.Plain, target: self, action: "showFriends:");
            
          //  self.deleteOld()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Friends", style: UIBarButtonItemStyle.Plain, target: self, action: "showFriends:");
      
            findFriends()
            self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil);
        }else{
            //Hide the Friends button when there's no current user
            self.navigationItem.rightBarButtonItem = nil
            self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil);
        }
    }
       
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            mapView.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
            mapView.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationmanager.startUpdatingLocation()
            let user = PFUser.currentUser()
            
            mapView.myLocationEnabled = true
        }
    }
    
//    override func viewWillDisappear(animated: Bool) {
//        mapView.removeObserver(self, forKeyPath: "myLocation", context: nil);
//        //mapView.delegate = nil;
//        
//        
//    }
    override func viewDidDisappear(animated: Bool) {
        mapView.removeObserver(self, forKeyPath: "myLocation", context: nil);

    }
    @IBAction func unwindToVC(segue:UIStoryboardSegue) {
        if(segue.sourceViewController .isKindOfClass(ShowRunViewController))
        {
//            self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil);
            
        }
    }

    
    @IBAction func changeMapType(sender: AnyObject) {
        let actionSheet = UIAlertController(title: "Map Types", message: "Select map type:", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let normalMapTypeAction = UIAlertAction(title: "Normal", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
            self.mapView.mapType = kGMSTypeNormal
            self.MapButton.setTitle("Normal", forState: .Normal)
        }
        
        let terrainMapTypeAction = UIAlertAction(title: "Terrain", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
            self.mapView.mapType = kGMSTypeTerrain
            self.MapButton.setTitle("Terrain", forState: .Normal)
        }
        
        let hybridMapTypeAction = UIAlertAction(title: "Hybrid", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
            self.mapView.mapType = kGMSTypeHybrid
            self.MapButton.setTitle("Hybrid", forState: .Normal)
        }
        
        let cancelAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
            
        }
        
        actionSheet.addAction(normalMapTypeAction)
        actionSheet.addAction(terrainMapTypeAction)
        actionSheet.addAction(hybridMapTypeAction)
        actionSheet.addAction(cancelAction)
        
        presentViewController(actionSheet, animated: true, completion: nil);
        
    }
    
    func showFriends(sender: AnyObject) {
    
        performSegueWithIdentifier("showFriends", sender: self)
    
    }
    
    


}
