//
//  ShowRunViewController.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/7/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import GoogleMaps
import HealthKit
import MapKit
import Parse

class ShowRunViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
 
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var averageSpeed: UILabel!

    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var maxSpeed: UILabel!
    
    @IBOutlet weak var altitudeChange: UILabel!
    
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var timeStamp: UILabel!
    var objectIds = [String]()
    var a_s: String!
    var ms: String!
    var t: String!
    var ac: String!
    var d: String!
    var da: NSDate!
    var myLocations = [CLLocation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self;
        if (a_s == "nan"){
            averageSpeed.text = "0 m/s"
        }
        else {
        averageSpeed.text = a_s
        }
        let dateFormat = NSDateFormatter()
        dateFormat.dateStyle = NSDateFormatterStyle.MediumStyle
        
        time.text = t
        maxSpeed.text = ms
     
        altitudeChange.text = ac
        distance.text = d
        var date = dateFormat.stringFromDate(da);
        timeStamp.text = date
        if (!objectIds.isEmpty){
            for var i = 0; i < objectIds.count; i++ {
                self.fetchLocation(objectIds[i], count: i);
            }
        }
        loadMap();
        
    }
    
    func mapRegion() -> MKCoordinateRegion {
        let initialLoc = myLocations.first
        
        var minLat = initialLoc?.coordinate.latitude
        var minLng = initialLoc?.coordinate.longitude
        var maxLat = minLat
        var maxLng = minLng
        
        let locations = myLocations
        
        for location in locations {
            minLat = min(minLat!, location.coordinate.latitude)
            minLng = min(minLng!, location.coordinate.longitude)
            maxLat = max(maxLat!, location.coordinate.latitude)
            maxLng = max(maxLng!, location.coordinate.longitude)
        }
        
        return MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: (minLat! + maxLat!)/2,
                longitude: (minLng! + maxLng!)/2),
            span: MKCoordinateSpan(latitudeDelta: (maxLat! - minLat!)*1.1,
                longitudeDelta: (maxLng! - minLng!)*1.1))
    }
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if !overlay.isKindOfClass(MKPolyline) {
            return nil
        }
        
        let polyline = overlay as! MKPolyline
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = UIColor.blackColor()
        renderer.lineWidth = 3
        return renderer
    }
    
    func polyline() -> MKPolyline {
        var coords = [CLLocationCoordinate2D]()
        
        let locations = myLocations
        for location in locations {
            coords.append(CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude))
        }
        let cnt = myLocations.count
        let pl = MKPolyline(coordinates: &coords, count: cnt)
        
        return pl
    }
    
    func loadMap() {
        if myLocations.count > 0 {
            mapView.hidden = false
            
            
            mapView.region = mapRegion()
            
            
            let pl = polyline()
            mapView.addOverlay(pl)
        } else {

        }
    }
    
    func fetchLocation(objectid: String, count: Int){
        let query = PFQuery(className: "Location")
        query.whereKey("objectId", equalTo: objectid)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        print(object.objectId)
                        let temp = CLLocation(latitude: object["latitude"] as! Double,
                            longitude: object["longitude"] as! Double)
                        self.myLocations.append(temp);
                        if (count == self.objectIds.count-1){
                            self.loadMap()
                        }
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        
        
    }
 
}
