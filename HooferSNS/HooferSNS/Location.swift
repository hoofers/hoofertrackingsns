//
//  Location.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/7/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import Foundation


class Location {
    
    var timestamp = NSDate()
    var latitude = 0.0
    var longitude = 0.0
    var run = Run();
    
}

