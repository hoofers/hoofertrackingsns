//
//  GPSTableViewController.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/6/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//


import UIKit
import CoreLocation

class GPSTableViewController: UITableViewController, CLLocationManagerDelegate {

    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var userSpeed: UILabel!
    @IBOutlet weak var altLabel: UILabel!
    @IBOutlet weak var userAltitude: UILabel!
    var locationmanager : CLLocationManager!
    
    var firstLocationUpdate: Bool?
    
    var didFindMyLocation = false
    
    @IBOutlet var tableData: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationmanager = CLLocationManager()
        self.locationmanager.delegate = self;
        self.locationmanager.requestWhenInUseAuthorization();
        self.locationmanager.distanceFilter = 10
        self.locationmanager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationmanager.startUpdatingLocation()
        tableView.backgroundColor = UIColor.clearColor();
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationmanager.startUpdatingLocation()
            let speed = (Int)(manager.location!.speed)
            let alt = (Int)(manager.location!.altitude)
            userAltitude.text = (String)(alt) + "meters";
            if (speed == -1) {
                userSpeed.text = "0k/h"
            } else {
                userSpeed.text = (String)(speed) + "k/h"
            }
            
        }
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }



}
