//
//  LogInViewController.swift
//  HooferSNS
//
//  Created by Gaoyuan Chen on 12/2/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import Parse


class LogInViewController: UIViewController {
    
    @IBAction func signIn(sender: AnyObject) {
        
        let usernameText = usernameField.text!
        let passwordText = passwordField.text!
        
        PFUser.logInWithUsernameInBackground(usernameText, password:passwordText) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                
                self.navigationController?.navigationBarHidden = false
                self.navigationController?.popToRootViewControllerAnimated(true)
                
                
            } else {
                // The login failed alert.
                let alert = UIAlertController(title: "Unable to Log In", message:  "Username/Password Incorrect", preferredStyle:  UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
   
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.

//        navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: true)
        
        //Set background to image
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "signInBackground")!)

    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}