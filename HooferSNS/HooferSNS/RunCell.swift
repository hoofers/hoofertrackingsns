//
//  RunCell.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/13/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit

class RunCell: UITableViewCell {
    
    
    @IBOutlet weak var run: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
