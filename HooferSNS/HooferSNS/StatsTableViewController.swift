//
//  StatsTableViewController.swift
//  HooferSNS
//
//  Created by Cormick Hnilicka on 12/10/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import Parse
import CoreLocation

class StatsTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    var numberRuns = 0;
    
    var runs = [PFObject]()
    var locations = [CLLocation]()
    var count = 0
    
    @IBOutlet var tableData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableData.delegate = self;
        tableView.backgroundColor = UIColor.clearColor();
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if(PFUser.currentUser() != nil){
            
        let query = PFQuery(className: "Run");
        let u = PFUser.currentUser()?.objectId
        query.whereKey("owner", equalTo: u!)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                self.runs.removeAll()
                self.count = 0
                print("Successfully retrieved \(objects!.count) runs.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        self.runs.append(object)
                        print(object.objectId)
                        self.count++;
                        self.tableView.reloadData();
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        }else{
        
            let alert = UIAlertController(title: "Sign in?", message: "You have to sign in to continue this page", preferredStyle: .Alert)
            let firstAction = UIAlertAction(title: "Cancel", style: .Default) { (alert: UIAlertAction!) -> Void in
                self.tabBarController?.selectedIndex = 0;
            }
            
            let secondAction = UIAlertAction(title: "Sign In", style: .Default) { (alert: UIAlertAction!) -> Void in
                self.performSegueWithIdentifier("signInFromStats", sender: self)
            }
            
            alert.addAction(firstAction)
            alert.addAction(secondAction)
            presentViewController(alert, animated: true, completion:nil)
        }
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let user = PFUser.currentUser();
        
        let n = (self.runs.count)
        return n;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "selectedRun"){
            let path = self.tableData.indexPathForSelectedRow
            let num = path?.row
            let run = self.runs[num!]
            let taskvc : ShowRunViewController = segue.destinationViewController as! ShowRunViewController
            
            taskvc.a_s = run["avgSpeed"] as! String;
            taskvc.ms = run["maxSpeed"] as! String;
            taskvc.d = String(run["distance"]);
            taskvc.ac = run["altChange"] as! String;
            taskvc.t = String(run["duration"]);
            taskvc.da = run["timestamp"] as! NSDate;
            
            var ids = run["locations"] as! [AnyObject];
            
            var idsToPass = [String]()
            
           
            for var i = 0; i < ids.count; i++ {
                var temp = ids[i]
                var q = temp.valueForKey("objectId") as! String
                
                
               idsToPass.append(q)
            }
            
         
            
            
            taskvc.objectIds = idsToPass;

        }
        
    }
    
    
    
//    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
//        var i = indexPath!.row
//        self.performSegueWithIdentifier("selectedRun", sender: self)
//    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("runCell", forIndexPath: indexPath) as! RunCell
        
        cell.run.text = runs[indexPath.row].objectId
        
        cell.backgroundColor = UIColor.clearColor()
        
        // Configure the cell...
        
        return cell
    }
}
