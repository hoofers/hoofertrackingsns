//
//  LogOutViewController.swift
//  HooferSNS
//
//  Created by Gaoyuan Chen on 12/6/15.
//  Copyright © 2015 Gaoyuan Chen. All rights reserved.
//

import UIKit
import Parse

class LogOutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: true)
        
        PFUser.logOut()
        var currentUser = PFUser.currentUser() // this will now be nil
        // Do any additional setup after loading the view.
        
        //Update UI - Change Background to image and adjust Nav Bar
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "logOutBackground")!)
        self.navigationController?.navigationBar.backgroundColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
